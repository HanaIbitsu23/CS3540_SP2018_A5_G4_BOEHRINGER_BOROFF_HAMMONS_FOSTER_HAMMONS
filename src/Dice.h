#ifndef DICE_H_
#define DICE_H_
#include"Die.h"

class Dice {

private:
	Die die1;
	Die die2;
	Die die3;
	Die die4;
	Die die5;
	int values[5] = { 0, 0, 0, 0, 0 };

public:
	Dice();
	void rollDice();
	void getDice(int[]);
	void sortDice();
};
#endif
