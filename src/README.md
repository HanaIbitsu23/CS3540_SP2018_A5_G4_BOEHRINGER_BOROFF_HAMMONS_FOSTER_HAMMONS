Rules for Yahtzee - Single Player

Welcome! This program is designed to allow a player to play through an entire 
game of Yahtzee. The only difference between this game and the original is that 
the play will not be allowed to reroll any of the dice and you will roll so that 
all score options are filled (there is no "No Score").

Instructions:
1. Roll the dice.
2. Add up the score of the dice.
3. Determine the type of combo that you are going to use for the turn. These 
include: Aces, Twos, Threes, Fours, Fives, and Sixes as well as combos such
as Three of a Kind, Four of a Kind, Full House, Small Straight, Full Straight,
Yahtzee, and Chance.
4. Repeat.

Repeat the instructions above for thirteen rounds. If you cannot fill out a 
combo the game will prompt you to add zero to your score. If you can, determined
by the combo that you choose, you will have a set amount of points added to your
score. 

Scoring:

-The upper section consists of totaling your five dice based on how many of each
number that you get.
-For example, if you roll 3 on three of the dice, you would total all five die
and record the score in the three's sections of the scorecard. Repeat this until
all of the numbers are accounted for.
-If you score above 63 points in the upper section, you will recieve a 35 point
bonus.
-The bottom section consists of the combos, including Three of a Kind, Four of
a Kind, Full House, Small Straight, Large Straight, Yahtzee, and Chance. Scoring
is as follows:
    *Three of a Kind - total of all die
    *Four of a Kind - total of all die
    *Full House - 25
    *Small Straight - 30
    *Large Straight - 40
    *Yahtzee - 50
    *Chance - total of all die
    
    **if you recieve more that one Yahtzee, you will recieve a 100 point bonus
    
-Once 13 round have passes, total up the upper and bottom sections and that is 
your score.


At the end of each game, you will be asked if you would like to play again.
