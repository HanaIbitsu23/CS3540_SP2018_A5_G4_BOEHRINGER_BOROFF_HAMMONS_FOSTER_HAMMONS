#include"dice.h"

Dice::Dice() {

}

void Dice::rollDice() {
	die1.roll();
	die2.roll();
	die3.roll();
	die4.roll();
	die5.roll();

	values[0] = die1.getValue();
	values[1] = die2.getValue();
	values[2] = die3.getValue();
	values[3] = die4.getValue();
	values[4] = die5.getValue();

	sortDice();
}

void Dice::getDice(int arr[])
{
	for (int i = 0; i < 5; i++)
	{
		arr[i] = values[i];
	}
}

void Dice::sortDice()
{
	int i, j, n, temp;
	
	n = sizeof(values) / sizeof(values[0]);

	for (i = 0; i < n - 1; i++)
	{
		for (j = 0; j < n - i - 1; j++)
		{
			if (values[j] > values[j + 1])
			{
				temp = values[j];
				values[j] = values[j + 1];
				values[j + 1] = temp;
			}
		}
	}
}