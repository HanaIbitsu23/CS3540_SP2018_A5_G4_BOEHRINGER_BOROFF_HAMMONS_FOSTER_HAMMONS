#include "Scorecard.h"
#include <iostream>

const int NUM_DICE = 5;

// Constructor
Scorecard::Scorecard()
{
	choice = 0;
}

// Main function
void Scorecard::recordScore(int d[])
{
	for (int i = 0; i < NUM_DICE; i++) // Assigns dice values to the member variable array
		dice[i] = d[i];

	getChoice(); 

	switch (choice)
	{
	case 1:
		
	case 2:
		
	case 3:
		
	case 4:
		
	case 5:
		
	case 6:
		scoreUpper();
		break;
	case 7:
		scoreThreekind();
		break;
	case 8:
		scoreFourkind();
		break;
	case 9:
		scoreFullhouse();
		break;
	case 10:
		scoreSmall();
		break;
	case 11:
		scoreLarge();
		break;
	case 12:
		scoreYahtzee();
		break;
	case 13:
		scoreChance();
		break;
	}

	displayScores();

	return;
}


// Displays scores
void Scorecard::displayScores()
{
	std::cout << std::endl << std::endl;
	std::cout << "Current Scores:\n-1 indicates an unscored category.\n";
	std::cout << "---------------------------" << std::endl;
	std::cout << "Upper Section" << std::endl;
	std::cout << "Aces:\t" << scores[1] << std::endl;
	std::cout << "Twos:\t" << scores[2] << std::endl;
	std::cout << "Threes:\t" << scores[3] << std::endl;
	std::cout << "Fours:\t" << scores[4] << std::endl;
	std::cout << "Fives:\t" << scores[5] << std::endl;
	std::cout << "Sixes:\t" << scores[6] << std::endl;
	std::cout << "Total of Upper Section:\t" << scores[15] << std::endl;
	std::cout << "---------------------------" << std::endl;
	std::cout << "Lower Section" << std::endl;
	std::cout << "Three of a Kind:\t" << scores[7] << std::endl;
	std::cout << "Four of a Kind:\t\t" << scores[8] << std::endl;
	std::cout << "Full House:\t\t" << scores[9] << std::endl;
	std::cout << "Small Straight:\t\t" << scores[10] << std::endl;
	std::cout << "Large Straight:\t\t" << scores[11] << std::endl;
	std::cout << "Yahtzee:\t\t" << scores[12] << std::endl;
	std::cout << "Chance:\t\t\t" << scores[13] << std::endl;
	std::cout << "Number of Yahtzee Bonuses:\t" << scores[14] << " times 100 = " << scores[14] * 100 << std::endl;
	std::cout << "Total of Lower Section:\t" << scores[16] << std::endl;
	std::cout << "\n\n";

	return;
}

// Gets the user's choice for scoring
void Scorecard::getChoice()
{
	bool valid = false;

	std::cout << "Face values:" << std::endl;
	for (int i = 0; i < NUM_DICE; i++) // Lists the current values of the dice
	{
		std::cout << dice[i] << ", ";
	}
	std::cout << "\nEnter the number of what you want to score." << std::endl;
	std::cout << "1. Aces \n2. Twos \n3. Threes \n4. Fours \n5. Fives \n6. Sixes \n";
	std::cout << "7. Three of a Kind \n8. Four of a Kind \n9. Full House \n10. Small Straight \n11. Large Straight \n12. Yahtzee \n13. Chance" << std::endl;

	do // Checks if choice is a) 1-14 and b) unscored
	{
		std::cin >> choice;

        // Second half or OR condition checks for bonus Yahtzee
        // Conditions:
        // 1) The user is choosing to score Yahtzee
        // 2) The number of bonus Yahtzees is < 3
        // 3) The roll is a Yahtzee
        // 4) The Yahtzee category is already scored 50 (a 0 score means no bonus Yahtzees allowed)
		if (choice > 0 && choice < 15 && scores[choice] == -1 || (choice == 12 && scores[14] < 3 && dice[0] == dice[4] && scores[12] == 50))
			valid = true;                                                                                                               
		else std::cout << "That category has already been scored. Please choose another." << std::endl;
	} while (!valid);

	return;
}

// Scores point in the upper half of the board.
// Also updates upper score total
void Scorecard::scoreUpper()
{
	int total = 0;

	for (int i = 0; i < NUM_DICE; i++)
	{
		if (dice[i] == choice) // If the value of a die == the choice, it adds that value to the total
			total += choice;   // Works because the number for the choice corresponds to the category name and where it's stored in scores[]
	}

	scores[choice] = total;
	scores[15] += total;

	return;
}

//***For lower scoring, if the current roll isn't elegible for the choice,
//***the category will be scored 0.
//***Both the category score and the total lower score are updated in each function
//***for the purposes of displaying the scores properly.

// Scores three of a kind
void Scorecard::scoreThreekind()
{
	int total = 0;

	if (dice[0] == dice[2] || dice[1] == dice[3] || dice[2] == dice[4]) // Checks for three of a kind (sorted array)
	{
		for (int i = 0; i < NUM_DICE; i++) // Adds face values of all dice
		{
			total += dice[i];
		}
		scores[7] = total;
		scores[16] += total;
	}
	
	else scores[7] = 0;


	return;
}

// Scores four of a kind
void Scorecard::scoreFourkind()
{
	int total = 0;

	if (dice[0] == dice[3] || dice[1] == dice[4]) // Checks for four of a kind
	{
		for (int i = 0; i < NUM_DICE; i++) // Adds total of face values
		{
			total += dice[i];
		}
		scores[8] = total;
		scores[16] += total;
	}

	else scores[8] = 0;

	return;
}

// Scores full house (always 25)
void Scorecard::scoreFullhouse()
{
	if (dice[0] == dice[1] && dice[2] == dice[4] && dice[1] != dice[2] || dice[4] == dice[3] && dice[2] == dice[0] && dice[3] != dice[2]) // Checks for a full house
	{
		scores[9] = 25;
		scores[16] += 25;
	}
	else scores[9] = 0;

	return;
}

// Scores small straight (30 points)
void Scorecard::scoreSmall()
{
	if (dice[1] == dice[0] + 1 && dice[2] == dice[1] + 1 && dice[3] == dice[2] + 1 || dice[2] == dice[1] + 1 && dice[3] == dice[2] + 1 && dice[4] == dice[3] + 1) // Checks for small straight
	{
		scores[10] = 30;
		scores[16] += 30;
	}
	else scores[10] = 0;

	return;
}

// Scores large straight (40 points)
void Scorecard::scoreLarge()
{
	if (dice[1] == dice[0] + 1 && dice[2] == dice[1] + 1 && dice[3] == dice[2] + 1 && dice[4] == dice[3] + 1) // Checks for large straight
	{
		scores[11] = 40;
		scores[16] += 40;
	}
	else scores[11] = 0;

	return;
}

// Scores Yahtzees. This was annoying to figure out.
void Scorecard::scoreYahtzee()
{
	bool valid = false;

    // First Yahtzee
	if (dice[0] == dice[4] && scores[12] == -1) // Checks for five of a kind and for no score
	{
		scores[12] = 50;
		scores[16] += 50;
	}

	else if (dice[0] != dice[4] && scores[12] == -1) // Checks for no Yahtzee and for no score
	{
		scores[12] = 0;
	}
	
	// Bonus Yahtzee scoring cases:
	// 1. Corresponding upper section box (face value * 5)
	// 2. Lower section box (follow scoring rules)
	// 3. Another upper section box (score 0)
	// Each case scores the proper section and increments the number of bonus Yahtzees

    // The getChoice() function validates the dice for a bonus Yahtzee. It shouldn't be possible to get here
    // if either a) Yahtzee hasn't been scored or b) bonus Yahtzee is ineligible.
	else
	{
		if (scores[dice[0]] == -1) // Case 1 - if corresponding upper section hasn't been scored
		{
			scores[dice[0]] = dice[0] * 5; // Corresponding upper section score is face value * 5 (total of all dice)
			scores[14]++;

			std::cout << "Bonus Yahtzee! Your face value category has been scored." << std::endl;
		}
		else if (scores[7] == -1 || scores[8] == -1 || scores[9] == -1 || scores[10] == -1 || scores[11] == -1 || scores[13] == -1) // Case 2 - if any of the bottom categories aren't scored
		{
			std::cout << "Bonus Yahtzee! Choose a bottom category to be scored." << std::endl;
			std::cout << "7. Three of a Kind \n8. Four of a Kind \n9. Full House \n10. Small Straight \n11. Large Straight \n13. Chance" << std::endl;

			do // Gets choice and checks for validity
			{
				std::cin >> choice; // Gets a new choice

				if (choice < 7 || choice > 11 && choice != 13) // If choice isn't listed above
				{
					std::cout << "Choose a bottom category." << std::endl;
				}
				else if (scores[choice] != -1) // If choice has already been scored
				{
					std::cout << "That category has already been scored." << std::endl;
				}
				else if (scores[choice] == -1) // If choice has not been scored
				{
					valid = true;
				}
			} while (!valid);

			switch (choice) // Scores corresponding category - the three functions that could be called here don't rely on the choice, only the dice
			{
			case 7:
				scoreThreekind();
				break;
			case 8:
				scoreFourkind();
				break;
			case 9:
				scores[9] = 25;
				scores[16] += 25;
				break;
			case 10:
				scores[10] = 30;
				scores[16] += 30;
				break;
			case 11:
				scores[11] = 40;
				scores[16] += 40;
				break;
			case 13:
				scoreChance();
				break;
			}

			scores[14]++;
		}
		else // Case 3 - if the corresponding top category is scored and all bottom categories are scored
		{
			std::cout << "Bonus Yahtzee! Choose a top category to be scored 0." << std::endl;
			std::cout << "1. Aces \n2. Twos \n3. Threes \n4. Fours \n5. Fives \n6. Sixes \n";
			
			do // Gets choice and checks validity
			{
				std::cin >> choice;

				if (choice < 1 || choice > 6) // If choice is not an upper category
					std::cout << "Choose a top category." << std::endl;
				else if (scores[choice] != -1) // If choice is already scored
					std::cout << "That category is already scored." << std::endl;
				else valid = true;

			} while (!valid);

			scores[choice] = 0;
			scores[14]++;
		}
	}
	return;
}

// Scores chance
void Scorecard::scoreChance()
{
	int total = 0;

	for (int i = 0; i < NUM_DICE; i++) // Adds total of all five dice
	{
		total += dice[i];
	}

	scores[13] = total;
	scores[16] += total;

	return;
}

// Does post-game operations; call after 13 turns
void Scorecard::finalScore()
{
    // Displays total upper score and adds 35 if it's greater than 63
	std::cout << "Total Upper Score:\t" << scores[15] << std::endl;
	if (scores[15] >= 63)
	{
		std::cout << "Upper score is 63! 35 Bonus points added." << std::endl;
		scores[15] += 35;
		std::cout << "Final Upper Score:\t" << scores[15] << std::endl;
	}

    // Displays total lower score and adds the scores of the bonus Yahtzees (if any)
	std::cout << "Total Lower Score:\t" << scores[16] << std::endl;
	std::cout << "Yahtzee Bonus:\t\t" << scores[14] << " times 100 = " << scores[14] * 100 << std::endl;

	scores[16] += scores[14] * 100;
	std::cout << "Final Lower Score:\t" << scores[16] << std::endl;

    // Adds upper and lower scores and displays it
	std::cout << "Grand Total:\t" << scores[15] + scores[16] << std::endl;

	clearScore();

	return;
}


void Scorecard::clearScore()
{
	for (int i = 1; i < 14; i++)
	{
		scores[i] = -1;
	}

	scores[14] = 0;
	scores[15] = 0;
	scores[16] = 0;

}
