#include "Game.cpp"
#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
	// Check to see if the player would like to play another game.
	char startGame; 
	Game newGame;

	cout << "Do you want to Yahtzee? Y/N: " << endl;
	cin >> startGame;

	while (startGame != 'y' && startGame != 'Y' && startGame != 'n' && startGame != 'N') 
	{
		cout << "Invalid input. Please try again." << endl;
		cout << "Do you want to play Yahtzee? Y/N: " << endl;
		cin >> startGame;
	}
	while (startGame == 'y' || startGame == 'Y') 
	{
		newGame.play();

		cout << "Would you like to play again? Y/N" << endl;
		cin >> startGame;
	}

	cout << "Thank you for playing!" << endl;
	system("pause");
	return 0;
}