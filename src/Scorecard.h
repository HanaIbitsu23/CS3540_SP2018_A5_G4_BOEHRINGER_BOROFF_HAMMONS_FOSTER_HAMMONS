// HOW TO USE

// After every roll, call the recordScore() function and pass a SORTED (ascending) array of dice (five elements of numbers 1-6).
// After 13 turns, call the finalScore() function to finalize the scoring and display it.
// The current score is printed after every turn. You can also call displayScore() if the user wants to display the score again.
// :)


#ifndef SCORECARD
#define SCORECARD

class Scorecard 
{
private:

	int dice[5];	// Dice in a SORTED ARRAY (ascending)
	int choice;		// Choice for scoring
	
	/*
	Array positions:
	0: blank (for convenience)
	1-6: aces through sixes
	7: three of a kind
	8: four of a kind
	9: full house
	10: small straight
	11: large straight
	12: yahtzee:
	13: chance
	14: yahtzee bonus number
	15: upper total
	16: lower total
	
	*/

	int scores[17] = { 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0 }; // Holds all scores

    // Scoring functions
	void scoreUpper();
	void scoreThreekind();
	void scoreFourkind();
	void scoreFullhouse();
	void scoreSmall();
	void scoreLarge();
	void scoreYahtzee();
	void scoreChance();
	
	// Input function
	void getChoice();
	void clearScore();


public:

	Scorecard();

	void recordScore(int[]);
	void displayScores();
	void finalScore();
};


#endif