#ifndef DIE_H_
#define DIE_H_

class Die {
private:
	int value;
public:
	Die()
	{
		value = 0;
	}
	void roll();
	void setRoll(int);
	int getValue();
};
#endif
