#ifndef GAME_
#define GAME_
#include "Scorecard.h"
#include "Dice.h"

class Game
{
private:
	Dice dice;
	Scorecard scorecard;

	int arr[5];

public:
	Game();
	void play();
};

#endif